<?php

namespace Drupal\webform_calculation_fields\Form;

use Drupal\webform_ui\Form\WebformUiElementDeleteForm;

/**
 * Overrides the form delete action validating permission before delete.
 */
class WebformCalculationFieldsFormDelete extends WebformUiElementDeleteForm {

  use WebformCalculationFieldsElementUiAccess;

}
