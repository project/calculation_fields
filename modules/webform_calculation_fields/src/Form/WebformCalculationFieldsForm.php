<?php

namespace Drupal\webform_calculation_fields\Form;

use Drupal\webform_ui\Form\WebformUiElementEditForm;

/**
 * Overrides the form element checking the permission before delete the element.
 */
class WebformCalculationFieldsForm extends WebformUiElementEditForm {

  use WebformCalculationFieldsElementUiAccess;

}
