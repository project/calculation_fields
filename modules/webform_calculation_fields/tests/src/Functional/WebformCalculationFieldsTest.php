<?php

namespace Drupal\Tests\webform_calculation_fields\Functional;

/**
 * Validate all calculation field elements in webforms.
 */
class WebformCalculationFieldsTest extends WebformCalculationFieldElementBase {

  /**
   * Test webform_calculation_fields_examp with calculation fields.
   */
  public function testWebformCalculationFieldsExample() {
    $this->drupalGet('/webform/webform_calculation_fields_examp');
    $session = $this->getSession();
    $page = $session->getPage();

    $page->findField('edit-first-value')->setValue("10");
    $page->findField('edit-second-value')->setValue("10");
    $this->assertEquals($page->findField('edit-first-second')->getValue(), 20);

    $page->findField('edit-multiply')->setValue("100");
    $this->assertEquals($page->findField('edit-multiply-result')->getValue(), 2000);

    $page->findField('edit-to-divide')->setValue("10");
    $this->assertEquals($page->findField('edit-multiply-result-to-divide')->getValue(), 200);

    $page->find('css', 'input[value="Submit"]')->submit();
    $this->assertSession()->pageTextContains('New submission added to Webform Calculation fields example.');

    // Validate calculation field submission data.
    $this->drupalGet('/admin/structure/webform/manage/webform_calculation_fields_examp/submission/1/edit');
    $this->assertEquals($page->findField('edit-first-second')->getValue(), 20);
    $this->assertEquals($page->findField('edit-multiply-result')->getValue(), 2000);
    $this->assertEquals($page->findField('edit-multiply-result-to-divide')->getValue(), 200);
  }

  /**
   * Test loan_repayment_calculation_examp with calculation fields.
   */
  public function testWebformLoanRepaymentExample() {
    $this->drupalGet('/webform/loan_repayment_calculation_examp');
    $session = $this->getSession();
    $page = $session->getPage();

    $page->findField('edit-loan-amount')->setValue("10000");
    $page->findField('edit-interest-rate')->setValue("1.5");
    $page->findField('edit-loan-term')->setValue("60");
    $this->assertEquals(19000.00, $page->findField('edit-result')->getValue());
    $page->find('css', 'input[value="Submit"]')->submit();
    $this->assertSession()->pageTextContains('New submission added to Loan Repayment Calculation Example.');

    // Validate calculation field submission data.
    $this->drupalGet('/admin/structure/webform/manage/loan_repayment_calculation_examp/submission/1/edit');
    $this->assertEquals("19,000.00", $page->findField('edit-result')->getValue());
  }

  /**
   * Test multiple step webform examples.
   */
  public function testWebformMultipleSteps() {
    $this->drupalGet('/webform/webform_calculation_fields_multi');
    $session = $this->getSession();
    $page = $session->getPage();

    // Step 1.
    $page->findField('first_value')->setValue("2");
    $page->findField('second_value')->setValue("2");
    $this->assertEquals("40", $page->findField('first_two_10')->getValue());
    $this->assertEquals("0.04", $page->findField('first_two_100')->getValue());
    $page->findById('edit-wizard-next')->click();

    // Step 2.
    $this->assertEquals("4", $page->findField('first_second')->getValue());
    $this->assertEquals("40", $page->findField('first_two_100_1000')->getValue());
    $page->findById('edit-wizard-next')->click();

    // Step 3.
    $this->assertEquals("400", $page->findField('evalute_from_calculation_fields_value')->getValue());
    $page->findField('multiply')->setValue("1000");
    $this->assertEquals("4000", $page->findField('multiply_result')->getValue());
    $page->findField('to_divide')->setValue("2");
    $this->assertEquals('2000.00', $page->findField('multiply_result_to_divide')->getValue());

    $this->markTestIncomplete("Needs to revalidate the assertions below");

    // Submit data.
    // phpcs:disable
//    $page->findButton('edit-submit')->click();
//    $this->assertSession()->pageTextContains('New submission added to Webform Calculation fields multistep example.');
//
//    // Test stored values.
//    $this->drupalGet('/admin/structure/webform/manage/webform_calculation_fields_multi/submission/1/edit/all');
//
//    // Check all input populated.
//    $this->assertEquals("2", $page->findField('edit-first-value')->getValue());
//    $this->assertEquals("4", $page->findField('edit-second-value')->getValue());
//    $this->assertEquals("10", $page->findField('edit-multiply')->getValue());
//    $this->assertEquals("2", $page->findField('edit-to-divide')->getValue());
//
//    // Check calculated elements.
//    $this->assertEquals('30.00', $page->findField('edit-multiply-result-to-divide')->getValue());
//    $this->assertEquals("6", $page->findField('first_second')->getValue());
    // phpcs:enable
  }

  /**
   * Test webform with multiple steps.
   */
  public function testWebformMultipleStepsWithChanges() {
    $this->markTestSkipped("Needs to be refactored");
    $this->drupalGet('/webform/webform_calculation_fields_multi');
    $session = $this->getSession();
    $page = $session->getPage();

    $page->findField('edit-first-value')->setValue("4");
    $page->findById('edit-wizard-next')->click();

    // Populate step 2 and go back previous page.
    $page->findField('edit-second-value')->setValue("6");
    $page->findById('edit-wizard-prev')->click();

    // Change the first value populated and go to the next step.
    $page->findField('edit-first-value')->setValue("6");
    $page->findById('edit-wizard-next')->click();

    // The calculation edit-first-second must be calculated with new value.
    $this->assertEquals("12", $page->findField('edit-first-second')->getValue());

    // Go to the next steps, do not populate them and back to previous one.
    $page->findById('edit-wizard-next')->click();
    $page->findById('edit-wizard-prev')->click();
    $this->assertEquals("12", $page->findField('edit-first-second')->getValue());

    // Now populate the 3 steps fields and submit.
    $page->findById('edit-wizard-next')->click();
    $page->findField('edit-multiply')->setValue("100");
    $this->assertEquals("1200", $page->findField('edit-multiply-result')->getValue());
    // Test calculated element if a dependent field was changed.
    $page->findField('edit-multiply')->setValue("1000");
    $this->assertEquals("12000", $page->findField('edit-multiply-result')->getValue());

    // Before submit, back to the first step and change the first value.
    $page->findById('edit-wizard-prev')->click();
    $page->findById('edit-wizard-prev')->click();
    $page->findField('edit-first-value')->setValue("44");

    // Go to the second step and check if the calculation worked.
    $page->findById('edit-wizard-next')->click();
    $this->assertEquals("50", $page->findField('edit-first-second')->getValue());
    $page->findById('edit-wizard-next')->click();
    $this->assertEquals("50000", $page->findField('edit-multiply-result')->getValue());
  }

}
