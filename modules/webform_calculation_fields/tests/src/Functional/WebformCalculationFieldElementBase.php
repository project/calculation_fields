<?php

namespace Drupal\Tests\webform_calculation_fields\Functional;

use Behat\Mink\Element\DocumentElement;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Base class to help test webform calculation field element.
 */
class WebformCalculationFieldElementBase extends WebDriverTestBase {

  /**
   * Modules to load.
   *
   * @var string[]
   */
  protected static $modules = [
    'user',
    'calculation_fields',
    'webform',
    'webform_ui',
    'webform_test_element',
    'webform_calculation_fields',
    'webform_calculation_fields_examples',
  ];

  /**
   * Default theme to use on the tests.
   *
   * @var string
   */
  protected $defaultTheme = 'stable9';

  /**
   * Current page on the webform browser test.
   *
   * @var \Behat\Mink\Element\DocumentElement
   */
  protected DocumentElement $page;

  /**
   * Current webform id.
   *
   * @var string
   */
  protected $webform;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $admin_user = $this->drupalCreateUser([], 'calculator-admin', TRUE);
    $this->drupalLogin($admin_user);
  }

  /**
   * Crate webform.
   *
   * @param string $title
   *   Create webform.
   */
  protected function createWebform($title): void {
    $this->drupalGet('/admin/structure/webform/add');
    $this->submitForm(['title' => $title], 'Save');
    $this->assertSession()->pageTextContains("Webform {$title} created.");
    $this->webform = strtolower(str_replace(' ', '_', $title));
  }

  /**
   * Crate a webform field based on the params.
   *
   * @param string $name
   *   The field name.
   * @param string $type
   *   The type of the field.
   * @param array $settings
   *   Field settings.
   */
  protected function createField($name, $type, $settings = []): void {
    $this->drupalGet("/admin/structure/webform/manage/{$this->webform}/element/add/{$type}");
    $this->page->findField('title')->setValue($name);
    if (!empty($settings['required'])) {
      $this->page->find('css', 'details#edit-validation')->click();
      $this->page->findField('edit-properties-required')->check();
    }
    if (!empty($settings['el_settings'])) {
      foreach ($settings['el_settings'] as $prop_name => $value) {
        $this->page->find('css', "[name='properties[{$prop_name}]']")
          ->setValue("{$value}");
      }
    }
    $this->page->findButton('Save')->click();
    $this->assertSession()->pageTextContains("{$name} has been created.");
  }

  /**
   * Get page variable.
   *
   * @return \Behat\Mink\Element\DocumentElement
   *   Return the current page.
   */
  public function getPage(): DocumentElement {
    return $this->page;
  }

  /**
   * Set page variable.
   *
   * @param \Behat\Mink\Element\DocumentElement $page
   *   Page document element.
   */
  public function setPage($page) {
    $this->page = $page;
  }

}
