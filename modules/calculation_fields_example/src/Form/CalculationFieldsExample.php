<?php

namespace Drupal\calculation_fields_example\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a calculation_fields form.
 */
class CalculationFieldsExample extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'calculation_fields_example';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['first_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First value'),
      '#required' => TRUE,
    ];

    $form['second_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Second value'),
      '#required' => TRUE,
      '#min' => 0,
      '#max' => 2000,
    ];

    $form['third_value'] = [
      '#type' => 'number',
      '#title' => $this->t('third value'),
      '#required' => TRUE,
      '#min' => 0,
      '#max' => 2000,
    ];

    $form['multiple'] = [
      '#type' => 'select',
      '#title' => $this->t('Multiple'),
      '#options' => [
        1 => 1,
        2 => 2,
        3 => 3,
      ],
      '#required' => TRUE,
      '#min' => 0,
      '#max' => 2000,
    ];

    $form['result'] = [
      '#type' => 'form_calculation_element',
      '#title' => $this->t('Result'),
      '#required' => TRUE,
      '#evaluation_fields' => '(:first_value + :second_value + :third_value) * :multiple',
      '#evaluation_decimals' => 1,
    ];

    $form['result_markup'] = [
      '#type' => 'form_calculation_markup',
      '#evaluation_fields' => '<h3>This is my test result divided by 2: R$ <span>{{ ((:first_value + :second_value + :third_value) * :multiple) / 2 }}</span></h3>',
      '#placeholder' => $this->t('With preview markup'),
      '#evaluation_decimals' => 2,
    ];

    $form['result_markup_1'] = [
      '#type' => 'form_calculation_markup',
      '#evaluation_fields' => '<h3>This is my test result: R$ <span>{{ (:first_value + :second_value + :third_value) * :multiple }}</span></h3>',
      '#evaluation_decimals' => 2,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    foreach (['result', 'result_markup', 'result_markup_1'] as $key) {
      $this->messenger()->addStatus($this->t('The result submitted was: @value', ['@value' => $form_state->getValue($key)]));
    }
  }

}
