<?php

namespace Drupal\Tests\calculation_fields\Functional;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test the entire form feature with the calculation elements.
 */
class CalculationFieldsFormTest extends WebDriverTestBase {

  /**
   * Modules to load.
   *
   * @var string[]
   */
  protected static $modules = [
    'user',
    'calculation_fields',
    'calculation_fields_example',
  ];

  /**
   * Default theme to use on the tests.
   *
   * @var string
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $admin_user = $this->drupalCreateUser([], 'admin-test', TRUE);
    $this->drupalLogin($admin_user);
  }

  /**
   * Test calculation field elements in the form built.
   */
  public function testCalculationFieldFormExample() {
    $this->drupalGet('/calculation-fields-example/form');
    $session = $this->getSession();
    $page = $session->getPage();

    $markupElement = $page->find('css', 'div[data-drupal-selector="calculation_markup_calculation-fields-example_first_value_second_value_third_value_multiple"]');

    $this->assertFalse($markupElement->find('css', '.js-math-result')->isVisible());
    $this->assertTrue($markupElement->find('css', '.js-math-placeholder')->isVisible());

    $page->findField('edit-first-value')->setValue("10");
    $page->findField('edit-second-value')->setValue("20");
    $page->findField('edit-third-value')->setValue("2");
    $page->findField('edit-multiple')->setValue("2");

    $result = $page->findField('edit-result')->getValue();
    $this->assertEquals($result, 64);

    $page->findField('edit-multiple')->setValue("3");
    $result = $page->findField('edit-result')->getValue();
    $this->assertEquals($result, 96);

    $page->findField('edit-first-value')->setValue("20");
    $page->findField('edit-third-value')->setValue("0");
    $result = $page->findField('edit-result')->getValue();
    $this->assertEquals($result, 120);

    $this->assertFalse($markupElement->find('css', '.js-math-placeholder')->isVisible());
    $this->assertTrue($markupElement->find('css', '.js-math-result')->isVisible());
    $this->assertEquals("<h3>This is my test result divided by 2: R$ <span>60.00</span></h3>", $markupElement->find('css', '.js-math-result')->getHtml());
  }

}
