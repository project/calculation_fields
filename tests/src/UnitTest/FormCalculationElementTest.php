<?php
// phpcs:ignoreFile

namespace Drupal\Tests\calculation_fields\UnitTest;

use Drupal\Tests\UnitTestCase;

/**
 * Tests the FormCalculationElement class.
 *
 * @coversDefaultClass \Drupal\calculation_fields\Element\FormCalculationElement
 *
 * @group calculation_fields
 */
class FormCalculationElementTest extends UnitTestCase {

  /**
   *
   *
   * @todo implement form test validation stuff.
   */
  public function testDumy() {
    $this->assertEquals(1, 1);
  }

}
