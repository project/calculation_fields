<?php

namespace Drupal\Tests\calculation_fields\UnitTest;

use Drupal\calculation_fields\CalculationFieldsHandlerExpression;
use Drupal\calculation_fields\InvalidExpressionException;
use Drupal\Tests\UnitTestCase;

/**
 * Test class CalculationFieldsHandlerExpression.
 *
 * @group calculation_fields
 */
class CalculationFieldsHandlerExpressionTest extends UnitTestCase {

  /**
   * CalculationFieldsHandlerExpression service.
   *
   * @var \Drupal\calculation_fields\CalculationFieldsHandlerExpression
   */
  protected CalculationFieldsHandlerExpression $handler;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->handler = new CalculationFieldsHandlerExpression();
    parent::setUp();
  }

  /**
   * Test evaluation result of the expression built.
   *
   * @dataProvider getEvaluationDataProvider
   */
  public function testEvaluation($expression, $result) {
    $this->assertEquals(
      $result,
      $this->handler->evaluate($expression)
    );
  }

  /**
   * Data provider for evaluation.
   */
  public function getEvaluationDataProvider() {
    return [
      ['1 + 2', 3],
      ['1 - 2', -1],
      ['1 * 2', 2],
      ['3 / 3', 1],
      ['30 / 9', 3.3333333333333335],
    ];
  }

  /**
   * Test FormCalculationElement::buildExpression.
   *
   * @dataProvider buildExpressionDataProvider
   */
  public function testBuildExpression($expression, $values, $expectedExpBuilded) {
    $result = $this->handler->buildExpression($expression, $values);
    $this->assertEquals($expectedExpBuilded, $result);
  }

  /**
   * Provide test for testBuildExpression.
   */
  public function buildExpressionDataProvider() {
    return [
      [':field1 + :field2', ['field1' => 1, 'field2' => 2], '1 + 2'],
      [':field1 * :field2', ['field1' => 1, 'field2' => 2], '1 * 2'],
      [':field1 - :field2', ['field1' => 1, 'field2' => 2], '1 - 2'],
      [':field1 / :field2', ['field1' => 1, 'field2' => 2], '1 / 2'],
      [':field1 / :field2', ['field1' => "2,900", 'field2' => 2], '2.900 / 2'],
      [':field1 / :field2', ['field1' => "2.900", 'field2' => 2], '2.900 / 2'],
      ['1,000.00 / 900,0', ['field1' => "2.900", 'field2' => 2], '1.000.00 / 900.0'],
      ['(:field1 / :field2) * :field3', ['field1' => 1, 'field2' => 2, 'field3' => 3], '(1 / 2) * 3'],
      // Test value nonexistent for field, then, should get the default value.
      [':field1|2 / :field2', ['field2' => 2], '2 / 2'],
      [':field1|2 / :field2|1', [], '2 / 1'],
      [':field1|1 / :field2', ['field1' => "", 'field2' => 2], '1 / 2'],
      // Should return null when there is value missing for a field.
      [':field1|2 / :field2', [], NULL],
    ];
  }

  /**
   * Tests the getFieldsFromExpression method.
   *
   * @dataProvider getFieldsFromExpressionDataProvider
   */
  public function testGetFieldsFromExpression($inputExpression, $expectedFields) {
    $result = $this->handler->getFieldsFromExpression($inputExpression);
    $this->assertEquals($expectedFields, $result);
  }

  /**
   * Provides data for the testgetFieldsFromExpression method.
   */
  public function getFieldsFromExpressionDataProvider() {
    return [
      // Test simple math expressions.
      [':field1 + :field2', ['field1', 'field2']],
      [':field1 - :field2', ['field1', 'field2']],
      [':field1 * :field2', ['field1', 'field2']],
      [':field1 / :field2', ['field1', 'field2']],
      [':field1|2 / :field2', ['field1|2', 'field2']],
      // Test advanced math expressions.
      ['(:field1 + :field2) / :field3', ['field1', 'field2', 'field3']],
      [
        ':field1 - :field2 + :field3 * :field4',
        ['field1', 'field2', 'field3', 'field4'],
      ],
      [
        '(:field1 * :field2) / ((:field3 * :field4) / :field1)',
        ['field1', 'field2', 'field3', 'field4'],
      ],
    ];
  }

  /**
   * Test validateExpression.
   *
   * @dataProvider dataValidationFunctionProvider
   */
  public function testValidationExpressionFunction($expression, $expectedResult) {
    if ($expectedResult === FALSE) {
      $this->expectException(InvalidExpressionException::class);
      $this->handler->validateExpression($expression);
      return;
    }
    $this->assertEquals($expectedResult, $this->handler->validateExpression($expression));
  }

  /**
   * Data provider for the validate expression method.
   */
  public function dataValidationFunctionProvider() {
    return [
      [':field1 + :field2', TRUE],
      [':fie___ld1 + :fie___ld2', TRUE],
      ['(:fie___ld1 + :fie___ld2)', TRUE],
      ['((:fie___ld1 / :field3) + :fie___ld2)', TRUE],
      ['(:fie___ld1 / :field3) + (:fie___ld2 * 10)', TRUE],
      ['10 * 10', TRUE],
      ['2.5 * 10', TRUE],
      ['2.5 * :field_1', TRUE],
      ['(2.5 * : q weq wfield_1) / 10', FALSE],
      [':field_valid * invalid_field', FALSE],
      [':field_valid * :invalid-field', FALSE],
      ['(field:valid * :invalid-field)', FALSE],
      [':field // :field2', FALSE],
      [':fiel d / :field2', FALSE],
      [':field / :field 2', FALSE],
    ];
  }

}
