<?php

namespace Drupal\calculation_fields\Tests\Kernel;

use Drupal\calculation_fields\Element\FormCalculationMarkupElement;
use Drupal\Tests\UnitTestCase;

/**
 * Test the evaluation of the form calculation element markup.
 */
class FormCalculationMarkupElementTest extends UnitTestCase {

  /**
   * Test expressions built with markup elements.
   *
   * @dataProvider testExtractEvaluationDataProvider
   *   All the scenarios to be tested.
   */
  public function testExtractEvaluation($expression, $expected) {
    $result = FormCalculationMarkupElement::extractEvaluation([
      'name' => $this->randomMachineName(),
      '#evaluation_fields' => $expression,
    ]);
    $this->assertEquals($expected, $result);
  }

  /**
   * Data provider to text extract evaluation.
   */
  public function testExtractEvaluationDataProvider() {
    return [
      [
        '<h3>You account amount is: {{ :result }}</h3>',
        ':result',
      ],
      [
        '<div class="container-wrapper"></div><h3>You account amount is: {{ :field1 * :field2 }}</h3></div>',
        ':field1 * :field2',
      ],
      [
        '<div class="container-wrapper"></div><h3>You account amount is: {{ :field1 * :field2 }}</h3></div>',
        ':field1 * :field2',
      ],
      [
        '<div class="container-wrapper"></div><h3>Invalid expression:  :field1 * :field2 </h3></div>',
        NULL,
      ],
    ];
  }

}
