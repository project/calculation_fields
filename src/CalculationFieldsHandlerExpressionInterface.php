<?php

namespace Drupal\calculation_fields;

/**
 * Describe how any handle must work in calculation expression.
 */
interface CalculationFieldsHandlerExpressionInterface {

  /**
   * Build expression replacing fields for the value.
   *
   * Example: (:field_name_1 + :field_name2) became (2 + 2).
   *
   * @param string $expression
   *   The string expression to be built.
   * @param array $values
   *   Array of key values, where the key is the field name followed by value.
   *
   * @return string|null
   *   The expression with the fields replaced by the values.
   *   If any field hasn't valued the expression will return null.
   */
  public function buildExpression(string $expression, array $values): string | NULL;

  /**
   * Get field names of the expression.
   *
   * @param string $expression
   *   The string expression.
   *
   * @return array
   *   An array with the field names.
   */
  public function getFieldsFromExpression(string $expression): array;

  /**
   * Extract info from the expression field.
   *
   * @param string $field
   *   Field expression info.
   *
   * @return array
   *   Array with field name and default value if defined.
   */
  public function getExpressionInfo(string $field): array;

  /**
   * Evaluate the expression return the math result.
   *
   * @param string $expression
   *   The expression built.
   *
   * @return int|float|null
   *   Return in and float in case success. Otherwise, null if any error.
   */
  public function evaluate(string $expression): int | float | NULL;

  /**
   * Validate if an expression has correct format.
   *
   * What is correct format?
   * All fields to be replaced must start with colon, like the example above.
   * -> expression: :field_name / :field_name_x
   * -> invalid: field_name / :field_name_x.
   *
   * @param string $expression
   *   The expression to be validated.
   *
   * @return bool
   *   True if the expression is valid. Otherwise, false.
   *
   * @throws \Drupal\calculation_fields\InvalidExpressionException
   */
  public function validateExpression(string $expression): bool;

}
