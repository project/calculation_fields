<?php
// @phpcs:ignoreFile
namespace Drupal\calculation_fields;

/**
 * Invalid expression exception.
 */
class InvalidExpressionException extends \Exception {

  public function __construct(string $message = "") {
    // @todo Improve the message returned.
    parent::__construct($message);
  }

}
