<?php

namespace Drupal\calculation_fields;

use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Default calculation expression handler.
 */
class CalculationFieldsHandlerExpression implements CalculationFieldsHandlerExpressionInterface {

  /**
   * {@inheritDoc}
   */
  public function buildExpression(string $expression, array $values): string | null {
    $fields = $this->getFieldsFromExpression($expression);
    $collectedValues = [];
    foreach ($fields as $field) {
      [
        'fieldName' => $fieldName,
        'defaultValue' => $defaultValue,
      ] = $this->getExpressionInfo($field);

      // @todo improve this validation. 0 is a valid value.
      $value = isset($values[$fieldName]) && $values[$fieldName] !== "" ? $values[$fieldName] : $defaultValue;
      if ($value === "" || $value === NULL) {
        continue;
      }
      $collectedValues[$field] = $value;
    }
    // Check if all fields in expression has value.
    if (count($collectedValues) !== count(array_unique($fields))) {
      return NULL;
    }
    $exp = $expression;
    foreach ($collectedValues as $field => $value) {
      $exp = str_replace(':' . $field, $value, $exp);
    }
    return str_replace(',', '.', $exp);
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldsFromExpression(string $expression): array {
    $expressionFields = explode(':', $expression) ?? [];
    $expressionFields = array_filter($expressionFields);
    $fields = [];
    foreach ($expressionFields as $field) {
      $items = explode(' ', $field);
      $items = array_map(function ($item) {
        return preg_replace("/[^a-zA-Z0-9_|]/", "", $item);
      }, $items);
      $filteredItems = array_filter($items, function ($item) {
        return !is_numeric($item) && !empty($item);
      });
      $fields = array_merge($fields, $filteredItems);
    }
    return array_unique($fields);
  }

  /**
   * {@inheritDoc}
   */
  public function getExpressionInfo(string $field): array {
    $defaultValue = NULL;
    $info = explode('|', $field);
    if (count($info) === 2) {
      $defaultValue = $info[1];
    }
    return [
      'fieldName' => $info[0],
      'defaultValue' => $defaultValue,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function evaluate(string $expression): int|float|null {
    return (new ExpressionLanguage())->evaluate($expression);
  }

  /**
   * {@inheritDoc}
   */
  public function validateExpression(string $expression): bool {
    $patternToExtractFields = '/:[a-zA-Z_][a-zA-Z0-9_]*/';
    preg_match_all($patternToExtractFields, $expression, $matches);

    // Expression with just numbers are valid as well.
    if ((empty($matches) || count($matches[0]) === 0) && str_word_count($expression) === 0) {
      return TRUE;
    }
    // Generate fake values for each field, then simulate the evaluation.
    // @todo improve the validation method.
    $values = [];
    foreach ($matches[0] as $field) {
      $values[str_replace(':', '', $field)] = rand(1, 5);
    }
    $prepareExpression = str_replace(':', '', $expression);
    try {
      (new ExpressionLanguage())->evaluate($prepareExpression, $values);
    }
    catch (\Exception $exception) {
      throw new InvalidExpressionException($exception->getMessage());
    }
    return TRUE;
  }

}
