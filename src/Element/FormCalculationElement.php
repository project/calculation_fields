<?php

namespace Drupal\calculation_fields\Element;

use Drupal\calculation_fields\CalculationFieldsTrait;
use Drupal\calculation_fields\InvalidExpressionException;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Number;

/**
 * Provide a custom form element that display a result of an expression.
 *
 * @FormElement("form_calculation_element")
 */
class FormCalculationElement extends Number {

  use CalculationFieldsTrait;

  /**
   * {@inheritDoc}
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#evaluation_fields'] = NULL;
    $info['#evaluation_fields_mask'] = NULL;
    $info['#process'] = [self::class . '::processFormCalculationElement'];
    $info['#element_validate'] = [self::class . '::validate'];
    return $info;
  }

  /**
   * Check if fields is ready to be validated.
   *
   * The conditions to be validated is:
   * - If has multiple pages;
   * --- The current element must have in current page.
   * --- Or in the previous pages.
   *
   * @param array $element
   *   Element to be validated.
   * @param array $complete_form
   *   Complete form data.
   *
   * @return bool
   *   True the form attend the conditions to be validated. Otherwise, false.
   */
  protected static function fieldIsReadyToValidate(array $element, array $complete_form): bool {
    $currentStep = $complete_form["progress"]["#current_page"] ?? NULL;
    if (is_null($currentStep)) {
      return TRUE;
    }
    $elementStepPageWeight = self::findStepPageOfElement($element, $complete_form);
    if (empty($complete_form['elements'][$currentStep]) || is_null($elementStepPageWeight)) {
      return FALSE;
    }
    $currentStepWeight = $complete_form['elements'][$currentStep]['#weight'];
    $calculationElementIsInTheCurrentPage = (int) (($elementStepPageWeight - $currentStepWeight) * 1000);
    // The field is ready to validate if is displayed in the current page.
    // Is ready to valida also if was displayed in the previous steps.
    return $calculationElementIsInTheCurrentPage == 1 || $currentStepWeight > $elementStepPageWeight;
  }

  /**
   * Find the step page of an element by the name.
   *
   * @param array $element
   *   The element to find the page step.
   * @param array $complete_form
   *   ALl elements of the form.
   *
   * @return int|float|null
   *   Return's the page element weight if found. Otherwise, null.
   */
  protected static function findStepPageOfElement(array $element, array $complete_form): int | float | null {
    $elementStepPage = $element["#webform_parent_key"];
    if (empty($elementStepPage)) {
      return NULL;
    }
    $parent = self::getElement($complete_form, $elementStepPage);
    // @todo move this validation to a service, then this validation can be extended.
    if ($parent['#type'] !== 'webform_wizard_page' && $parent["#webform_depth"] > 0) {
      return self::findStepPageOfElement($parent, $complete_form);
    }
    return $parent["#weight"];
  }

  /**
   * Validate form values.
   */
  private static function validateFormValues(FormStateInterface $form_state, $fieldsToValidate) {
    foreach ($fieldsToValidate as $fieldName) {
      $populatedValue = $form_state->getValue($fieldName);
      // @todo improve this validation.
      $numericValue = str_replace(',', '.', $form_state->getValue($fieldName));
      if (!empty($populatedValue) && !is_numeric($numericValue)) {
        $form_state->setErrorByName(
          $fieldName,
          t('Value of field @field must be numeric!',
            ['@field' => $fieldName],
            ['context' => 'calculation_fields']
          )
        );
      }
    }
    return $form_state->getErrors();
  }

  /**
   * Validate form math element callback.
   *
   * To avoid disruptive data from the client, every submit evaluate the
   * expression and set the correct/expected value for the element.
   */
  public static function validate(&$element, FormStateInterface $form_state, &$complete_form) {
    // @todo this validation should be moved to the webform element.
    if (!self::fieldIsReadyToValidate($element, $complete_form)) {
      return;
    }

    $calculationService = self::calculationFieldsHandlerService();
    // All values to be evaluated in the expression must be numerics.
    // @todo check if should be better to return only the field names in this function.
    // Since in many places we need to call the get expression info function.
    $fieldsToBeNumeric = $calculationService->getFieldsFromExpression($element["#evaluation_fields"]);
    $fieldsToBeNumeric = array_map(function ($field) {
      [
        'fieldName' => $fieldName,
      ] = self::calculationFieldsHandlerService()->getExpressionInfo($field);
      return $fieldName;
    }, $fieldsToBeNumeric);
    if (self::validateFormValues($form_state, $fieldsToBeNumeric)) {
      return;
    }

    try {
      // Validate the expression before build it.
      $calculationService->validateExpression($element["#evaluation_fields"]);
    }
    catch (InvalidExpressionException $invalidExpressionException) {
      $form_state->setError($element, $invalidExpressionException->getMessage());
      return;
    }

    // Build the expression to be evaluated.
    $expression = $calculationService->buildExpression($element["#evaluation_fields"], $form_state->getValues());
    if (empty($expression)) {
      return;
    }

    // Mathjs supports pow functions by the char ^
    // symfony/expression-languages support by **
    // Replace them before the php evaluation.
    $expression = str_replace('^', '**', $expression);
    try {
      $result = $calculationService->evaluate($expression);
    }
    catch (\Exception $exception) {
      $form_state->setError($element, t('Failed to process calculation, check if you populate the values correctly!'));
      \Drupal::logger('webform_calculation_fields')
        ->error("Failed to evaluate the following expression: (@expression). Error found: @error", [
          '@expression' => $expression,
          '@error' => $exception->getMessage(),
        ]);
      return;
    }

    if (!empty($element["#evaluation_decimals"])) {
      $result = number_format($result, $element["#evaluation_decimals"]);
    }
    $value = NestedArray::getValue($form_state->getValues(), $element['#parents']);
    if ($result !== $value) {
      $form_state->setValue($element["#name"], $result);
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function preRenderNumber($element) {
    if (empty($element["#evaluation_fields"])) {
      return $element;
    }
    $element['#attributes']['type'] = 'text';
    $element['#attributes']['readonly'] = 'readonly';
    Element::setAttributes($element, ['id', 'name', 'value', 'placeholder', 'size', 'fields']);
    static::setAttributes($element, ['form-number', 'js-form-math-element']);
    $element['#attributes']['data-evaluate-expression'] = $element["#evaluation_fields"];
    $element['#attributes']['data-evaluate-fields'] = self::calculationFieldsHandlerService()->getFieldsFromExpression($element["#evaluation_fields"]);
    $element['#attached']['library'][] = 'calculation_fields/calculation_fields';

    if (!empty($element["#evaluation_fields_mask"])) {
      $element['#attributes']['data-inputmask'] = $element["#evaluation_fields_mask"];
    }

    return $element;
  }

  /**
   * Process each form calculation element setting custom properties.
   *
   * Set two properties:
   * - data-evaluate-context: used to guarantee that the input will use values,
   * in the correct form context.
   * - data-evaluate-deep-fields: deep fields properties is used to store
   * values of the fields that are not being displayed like in a multistep
   * form in a webform for example.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   * @param array $form
   *   The form items.
   */
  public static function processFormCalculationElement(array $element, FormStateInterface $form_state, array $form) {
    $calculationService = self::calculationFieldsHandlerService();
    if (empty($element["#evaluation_fields"])) {
      return $element;
    }
    $element['#attributes']['data-evaluate-context'] = str_replace('_', '-', $form["#form_id"]);
    if (isset($element['#evaluation_decimals'])) {
      $element['#attributes']['data-evaluate-decimals'] = (int) $element['#evaluation_decimals'];
    }
    $dependencyFields = $calculationService->getFieldsFromExpression($element["#evaluation_fields"]);

    $hasMultipleSteps = $form["progress"]["#current_page"] ?? NULL;

    $dependenciesValues = [];
    foreach ($dependencyFields as $field) {
      [
        'fieldName' => $fieldName,
        'defaultValue' => $defaultValue,
      ] = $calculationService->getExpressionInfo($field);

      if (!empty($form['elements'])) {
        $dependentElement = self::getElement($form['elements'], $fieldName);
      }
      else {
        $dependentElement = self::getElement($form, $fieldName);
      }

      if (isset($dependentElement["#webform"]) &&
        ($dependentElement["#webform_parent_key"] !== $hasMultipleSteps || !$dependentElement['#access'])) {
        $dependenciesValues[$fieldName] = !empty($form_state->getValue($fieldName)) ? $form_state->getValue($fieldName) : $defaultValue;
      }
    }
    $element['#attributes']['data-evaluate-deep-fields'] = json_encode($dependenciesValues);
    return $element;
  }

  /**
   * Get from dependent field the name and default value info.
   *
   * @param string $dependentFieldInfo
   *   The field data from the expression.
   *
   * @return array
   *   Array with the field name and default value.
   */
  protected static function getFieldDependentData(string $dependentFieldInfo): array {
    $fieldData = explode('|', $dependentFieldInfo);
    return [
      'fieldName' => $fieldData[0],
      'defaultValue' => $fieldData[1] ?? NULL,
    ];
  }

}
